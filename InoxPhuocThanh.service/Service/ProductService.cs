﻿using AutoMapper;
using InoxPhuocThanh.Data.Interface;
using InoxPhuocThanh.Data.ModelData;
using InoxPhuocThanh.service.ModelService;
using InoxPhuocThanh.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Service
{

    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;
        public ProductService(IProductRepository productRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _mapper = mapper;
        }

        public List<ProductServiceModel> GetListProduct()
        {
            return _mapper.Map<List<ProductServiceModel>>(_productRepository.GetListProduct());
        }


        public List<ProductServiceModel> GetListProductByCategoryOrTypeName(string name, int category, int type, int pageSize, int pageNum)
        {
            return _mapper.Map<List<ProductServiceModel>>(_productRepository.GetListProductByCategoryOrTypeName(name, category, type, pageSize, pageNum));
        }

        public List<ProductServiceModel> GetListProductByTypeName(string name, int pageSize, int pageNum)
        {
            return _mapper.Map<List<ProductServiceModel>>(_productRepository.GetListProductByTypeName(name, pageSize, pageNum));
        }

        public List<ProductServiceModel> GetListProductByCategoryName(string name, int pageSize, int pageNum)
        {
            return _mapper.Map<List<ProductServiceModel>>(_productRepository.GetListProductByCategoryName(name, pageSize, pageNum));
        }

        public ProductServiceModel GetProductDetail(int id)
        {
            return _mapper.Map<ProductServiceModel>(_productRepository.GetProductDetail(id));
        }

        public List<ProductServiceModel> GetListProductNew()
        {
            return _mapper.Map<List<ProductServiceModel>>(_productRepository.GetListProductNew());
        }

        public List<CategoryServiceModel> GetListProductHome()
        {
            return _mapper.Map<List<CategoryServiceModel>>(_productRepository.GetListProductHome());
        }

        public List<ProductServiceModel> SearchProduct(string searchValue, int categorySearch, int pageSize, int pageNum)
        {
                return _mapper.Map<List<ProductServiceModel>>(_productRepository.SearchProduct(searchValue,categorySearch,pageSize,pageNum));   
        }
    }
}
