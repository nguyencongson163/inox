﻿using AutoMapper;
using InoxPhuocThanh.Data.Interface;
using InoxPhuocThanh.service.ModelService;
using InoxPhuocThanh.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Service
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IMapper _mapper;
        public CompanyService(ICompanyRepository companyRepository, IMapper mapper)
        {
            _companyRepository = companyRepository;
            _mapper = mapper;
        }
        public List<CompanyServiceModel> GetCompanyInformation()
        {
            return _mapper.Map<List<CompanyServiceModel>>(_companyRepository.GetCompanyInformation());
        }
    }
}
