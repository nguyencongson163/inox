﻿using AutoMapper;
using InoxPhuocThanh.Data.Interface;
using InoxPhuocThanh.service.Interface;
using InoxPhuocThanh.service.ModelService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;
        public CategoryService(ICategoryRepository categoryRepository, IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }
        public List<CategoryServiceModel> GetCategories()
        {
            return _mapper.Map<List<CategoryServiceModel>>(_categoryRepository.GetListCategory());
            //throw new NotImplementedException();
        }
    }
}
