﻿using AutoMapper;
using InoxPhuocThanh.Data.Interface;
using InoxPhuocThanh.service.Interface;
using InoxPhuocThanh.service.ModelService;
using InoxPhuocThanh.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.service.Service
{
    public class ProductTypeService : IProductTypeService
    {
        private readonly IProductTypeRepository _productTypeRepository;
        private readonly IMapper _mapper;
        public ProductTypeService(IProductTypeRepository productTypeRepository, IMapper mapper)
        {
            _productTypeRepository = productTypeRepository;
            _mapper = mapper;
        }

        public List<ProductTypeServiceModel> GetProductTypes()
        {
            return _mapper.Map<List<ProductTypeServiceModel>>(_productTypeRepository.GetProductTypes());
        }
    }
}
