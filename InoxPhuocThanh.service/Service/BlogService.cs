﻿using AutoMapper;
using InoxPhuocThanh.Data.Interface;
using InoxPhuocThanh.service.Interface;
using InoxPhuocThanh.service.ModelService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.service.Service
{
    public class BlogService : IBlogService
    {
        private readonly IBlogRepository _blogRepository;
        private readonly IMapper _mapper;
        public BlogService(IBlogRepository blogRepository, IMapper mapper)
        {
            _blogRepository = blogRepository;
            _mapper = mapper;
        }

        public List<BlogServiceModel> getBlogById(int Id, int pageSize, int pageNum)
        {
            return _mapper.Map<List<BlogServiceModel>>(_blogRepository.getBlogById(Id, pageSize, pageNum));
        }

        public List<BlogServiceModel> getListBlog()
        {
            return _mapper.Map<List<BlogServiceModel>>(_blogRepository.getListBlog());
        }
    }
}
