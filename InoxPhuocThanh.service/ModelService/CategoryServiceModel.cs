﻿using InoxPhuocThanh.Data.ModelData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.service.ModelService
{
  public  class CategoryServiceModel
    {
        public int Category_id { get; set; }
        public string Category_name { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
        public List<ProductTypeServiceModel> ProductType { get; set; }
        public List<ProductServiceModel> Product { get; set; }
    }
}
