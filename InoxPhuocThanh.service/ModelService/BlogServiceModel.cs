﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.service.ModelService
{
   public class BlogServiceModel
    {
        public int Blog_id { get; set; }
        public string Blog_name { get; set; }
        public string Blog_description { get; set; }
        public string Blog_summary { get; set; }
        public DateTime Blog_created_date { get; set; }
        public string Blog_author { get; set; }
        public string Blog_img { get; set; }
        public int CountNum { get; set; }
    }
}
