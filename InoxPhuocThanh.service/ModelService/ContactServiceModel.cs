﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.service.ModelService
{
   public class ContactServiceModel
    {
        public int Contact_id { get; set; }
        public string Fullname { get; set; }
        public string PhoneNumber { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Customer_email { get; set; }
        public DateTime Created_date { get; set; }
    }
}
