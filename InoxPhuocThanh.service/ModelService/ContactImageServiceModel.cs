﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.service.ModelService
{
   public class ContactImageServiceModel
    {
        public int Contact_img_id { get; set; }
        public int Contact_id { get; set; }
        public string Image { get; set; }
    }
}
