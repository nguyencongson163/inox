﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.service.ModelService
{
    class CustomerServiceModel
    {
        public int Customer_id { get; set; }
        public string Customer_name { get; set; }
        public string Customer_phonenumber { get; set; }
        public string Customer_address { get; set; }
        public string Customer_gmail { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
    }
}
