﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.service.ModelService
{
    class ProductImageServiceModel
    {
        public int Product_Img_Id { get; set; }
        public int Product_Id { get; set; }
        public string Product_Name { get; set; }
        public string Product_Image { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
    }
}
