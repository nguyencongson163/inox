﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.service.ModelService
{
    class OrderProductServiceModel
    {
        public int Item_id { get; set; }
        public int Order_id { get; set; }
        public int Product_id { get; set; }
        public bool IsDelete { get; set; }
    }
}
