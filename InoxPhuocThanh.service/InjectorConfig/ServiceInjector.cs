﻿using InoxPhuocThanh.service.Interface;
using InoxPhuocThanh.service.Service;
using InoxPhuocThanh.Service;
using InoxPhuocThanh.Service.Interface;
using SimpleInjector;

namespace InoxPhuocThanh.service.InjectorConfig
{
    public static class ServiceInjector
    {
        public static void Inject(Container container)
        {
            container.Register<IProductService, ProductService>();
            container.Register<ICategoryService, CategoryService>();
            container.Register<IProductTypeService, ProductTypeService>();
            container.Register<ICompanyService, CompanyService>();
            container.Register<IBlogService, BlogService>();
        }
    }
}