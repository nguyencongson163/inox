﻿using InoxPhuocThanh.Data.ModelData;
using InoxPhuocThanh.service.ModelService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Service.Interface
{
   public interface IProductTypeService
    {
       List<ProductTypeServiceModel> GetProductTypes();
    }
}
