﻿using InoxPhuocThanh.Data.ModelData;
using InoxPhuocThanh.service.ModelService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Service.Interface
{
    public interface IProductService
    {
        List<ProductServiceModel> GetListProduct();
        List<ProductServiceModel> GetListProductByCategoryOrTypeName(string name,int category, int type, int pageSize, int pageNum);
        List<ProductServiceModel> GetListProductByCategoryName(string name, int pageSize, int pageNum);
        List<ProductServiceModel> GetListProductByTypeName(string name, int pageSize, int pageNum);
        ProductServiceModel GetProductDetail(int id);
        List<ProductServiceModel> GetListProductNew();
        List<CategoryServiceModel> GetListProductHome();
        List<ProductServiceModel> SearchProduct(string searchValue, int categorySearch, int pageSize, int pageNum);
    }
}
