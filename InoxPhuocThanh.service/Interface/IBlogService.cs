﻿
using InoxPhuocThanh.service.ModelService;
using System.Collections.Generic;

namespace InoxPhuocThanh.service.Interface
{
    public interface IBlogService
    {
        List<BlogServiceModel> getListBlog();
        List<BlogServiceModel> getBlogById(int Id, int pageSize, int pageNum);
    }
}
