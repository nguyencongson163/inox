﻿using InoxPhuocThanh.service.ModelService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.service.Interface
{
   public interface ICategoryService
    {
        List<CategoryServiceModel> GetCategories();
    }
}
