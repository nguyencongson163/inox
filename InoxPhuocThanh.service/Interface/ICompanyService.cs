﻿using InoxPhuocThanh.service.ModelService;
using System;
using System.Collections.Generic;

namespace InoxPhuocThanh.Service.Interface
{
   public interface ICompanyService
    {
        List<CompanyServiceModel> GetCompanyInformation();
    }
}
