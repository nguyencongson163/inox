﻿using AutoMapper;
using InoxPhuocThanh.Data.ModelData;
using InoxPhuocThanh.service.ModelService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.service
{
    public class ServiceMappingProfile : Profile
    {
        public ServiceMappingProfile()
        {
            CreateMap<ProductServiceModel, ProductDataModel>();
            CreateMap<ProductDataModel, ProductServiceModel>();

            CreateMap<CategoryDataModel, CategoryServiceModel>();
            CreateMap<CategoryServiceModel, CategoryDataModel>();

            CreateMap<ProductTypeServiceModel, ProductTypeDataModel>();
            CreateMap<ProductTypeDataModel, ProductTypeServiceModel>();

            CreateMap<CompanyServiceModel, CompanyDataModel>();
            CreateMap<CompanyDataModel, CompanyServiceModel>();

            CreateMap<ContactImageServiceModel, ContactImageDataModel>();
            CreateMap<ContactImageDataModel, ContactImageServiceModel>();

            CreateMap<ContactDataModel, ContactServiceModel>();
            CreateMap<ContactServiceModel, ContactDataModel>();

            CreateMap<BlogDataModel, BlogServiceModel>();
            CreateMap<BlogServiceModel ,BlogDataModel>();
        }
    }
}
