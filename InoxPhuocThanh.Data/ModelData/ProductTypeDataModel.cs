﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Data.ModelData
{
   public class ProductTypeDataModel
    {
        public int Product_type_id { get; set; }
        public string Product_type_name { get; set; }
        public int Category_id { get; set; }
        public int Product_quantity { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
    }
}
