﻿using System;

namespace InoxPhuocThanh.Data.ModelData
{
    public class ProductDataModel
    {
        public int Product_id { get; set; }
        public int Product_category_id { get; set; }
        public int Product_type_id { get; set; }
        public string Product_name { get; set; }
        public float Product_price { get; set; }
        public string Product_image { get; set; }
        public DateTime Product_created_date { get; set; }
        public string Product_description { get; set; }
        public int Product_quantity { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
        public int CountNum { get; set; }
    }
}
