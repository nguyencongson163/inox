﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Data.ModelData
{
   public class OrderDataModel
    {
        public int Order_id { get; set; }
        public int Customer_id { get; set; }
        public DateTime Order_date { get; set; }
        public string Size_description { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
    }
}
