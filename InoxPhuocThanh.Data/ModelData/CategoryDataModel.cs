﻿using System;
using System.Collections.Generic;

namespace InoxPhuocThanh.Data.ModelData
{
    
    public class CategoryDataModel
    {
        public int Category_id { get; set; }
        public string Category_name { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
        public List<ProductTypeDataModel> ProductType { get; set; }
        public List<ProductDataModel> Product { get; set; }

    }
}
