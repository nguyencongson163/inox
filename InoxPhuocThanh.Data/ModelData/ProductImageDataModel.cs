﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Data.ModelData
{
   public class ProductImageDataModel
    {
        public int Product_Img_Id { get; set; }
        public int Product_Id { get; set; }
        public string Product_Name { get; set; }
        public string Product_Image { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }

    }
}
