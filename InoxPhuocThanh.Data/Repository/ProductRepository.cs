﻿using Dapper;
using InoxPhuocThanh.Data.Implement;
using InoxPhuocThanh.Data.Interface;
using InoxPhuocThanh.Data.ModelData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Data.Repository
{
    class ProductRepository : IProductRepository
    {
        public List<ProductDataModel> GetListProduct()
        {
            using (var connection = ConnectionProvider.Get())
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@pageSize", 2);
                param.Add("@pageNumber", 1);
                return connection.Query<ProductDataModel>("sp_getListProduct", param, commandType: CommandType.StoredProcedure).ToList();
            }
        }


        public List<ProductDataModel> GetListProductByCategoryOrTypeName(string name, int category_id, int type_id, int pageSize, int pageNum)
        {
            var model = (List<ProductDataModel>) null;
            if (name == null)
            {
                name = "";
            }
            using (var connection = ConnectionProvider.Get())
            {
                DynamicParameters param = new DynamicParameters();
          
                param.Add("@pageSize", pageSize);
                param.Add("@pageNumber", pageNum);
                if (category_id == 0 && type_id != 0)
                {
                    param.Add("@type_id", type_id);
                    model = connection.Query<ProductDataModel>("sp_getListProductByTypeId", param, commandType: CommandType.StoredProcedure).ToList();
                }
                else if(type_id == 0 && category_id != 0 )
                {
                    param.Add("@category_id", category_id);
                    param.Add("@searchValue", name);
                    model = connection.Query<ProductDataModel>("sp_getListProductByCategoryIdAndSearch", param, commandType: CommandType.StoredProcedure).ToList();
                } else
                {
                    param.Add("@category_type_name", name);
                    model = connection.Query<ProductDataModel>("sp_getListProductByCategoryOrType", param, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            return model;
        }

        public List<ProductDataModel> GetListProductByTypeName(string name, int pageSize, int pageNum)
        {
            if (name == null)
            {
                name = "";
            }
            using (var connection = ConnectionProvider.Get())
            {
                DynamicParameters param = new DynamicParameters();

                param.Add("@type_name", name);
                param.Add("@pageSize", pageSize);
                param.Add("@pageNumber", pageNum);
                var model = connection.Query<ProductDataModel>("sp_getListProductByType", param, commandType: CommandType.StoredProcedure).ToList();
                return model;
            }
        }
        public List<ProductDataModel> GetListProductByCategoryName(string name, int pageSize, int pageNum)
        {
            if (name == null)
            {
                name = "";
            }
            using (var connection = ConnectionProvider.Get())
            {
                DynamicParameters param = new DynamicParameters();

                param.Add("@category_name", name);
                param.Add("@pageSize", pageSize);
                param.Add("@pageNumber", pageNum);
                var model = connection.Query<ProductDataModel>("sp_getListProductByCategory", param, commandType: CommandType.StoredProcedure).ToList();
                return model;
            }
        }

        public ProductDataModel GetProductDetail(int id)
        {
            using (var connection = ConnectionProvider.Get())
            {

                DynamicParameters param = new DynamicParameters();
                param.Add("@product_id", id);
                var list = connection.QueryFirstOrDefault<ProductDataModel>("sp_getProductDetail", param, commandType: CommandType.StoredProcedure);
                return list;
            }
        }

        public List<ProductDataModel> GetListProductNew()
        {
            using (var connection = ConnectionProvider.Get())
            {
                var list = connection.Query<ProductDataModel>("sp_getListProductNew", commandType: CommandType.StoredProcedure).ToList();
                return list;
            }
        }

        public List<CategoryDataModel> GetListProductHome()
        {
            using (var connection = ConnectionProvider.Get())
            {
                var model = connection.QueryMultiple("sp_getListProductHome", commandType: CommandType.StoredProcedure);
                var categories = model.Read<CategoryDataModel>().ToList();

                var product = model.Read<ProductDataModel>().ToList();
                categories.ForEach(x =>
                {
                    x.Product = product.Where(c => x.Category_id == c.Product_category_id).ToList();
                });
                return categories;
            }
        }
        public List<ProductDataModel> SearchProduct(string searchValue, int categorySearch, int pageSize, int pageNum)
        {
            using (var connection = ConnectionProvider.Get())
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@searchValue", searchValue);
                param.Add("@categorySearch", categorySearch);
                param.Add("@pageSize", pageSize);
                param.Add("@pageNum", pageNum);
                var model = connection.Query<ProductDataModel>("sp_searchProductbyNameAndCategory", param, commandType: CommandType.StoredProcedure).ToList();

                return model;
            }
        }
    }
}
