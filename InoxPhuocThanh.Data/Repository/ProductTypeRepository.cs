﻿using Dapper;
using InoxPhuocThanh.Data.Implement;
using InoxPhuocThanh.Data.Interface;
using InoxPhuocThanh.Data.ModelData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace InoxPhuocThanh.Data.Repository
{
    public class ProductTypeRepository : IProductTypeRepository
    {
        public List<ProductTypeDataModel> GetProductTypes()
        {
            using (var connection = ConnectionProvider.Get())
            {
                return connection.Query<ProductTypeDataModel>("sp_getListProductypeByCategory", commandType: CommandType.StoredProcedure).ToList();
            }
        }
    }
}
