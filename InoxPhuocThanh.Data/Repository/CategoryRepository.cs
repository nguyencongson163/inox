﻿using Dapper;
using InoxPhuocThanh.Data.Implement;
using InoxPhuocThanh.Data.Interface;
using InoxPhuocThanh.Data.ModelData;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace InoxPhuocThanh.Data.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        public List<CategoryDataModel> GetListCategory()
        {
            using (var connection = ConnectionProvider.Get())
            {
                var model = connection.QueryMultiple("sp_getListCategory", commandType: CommandType.StoredProcedure);
                //var producttypes = connection.Query<CategoryDataModel>("sp_getListCategory", commandType: CommandType.StoredProcedure).ToList();
                var categories = model.Read<CategoryDataModel>().ToList();
               
                var producttypes = model.Read<ProductTypeDataModel>().ToList();
                categories.ForEach(x =>
                {
                    x.ProductType = producttypes.Where(c => x.Category_id == c.Category_id).ToList();
                });
               
                return categories;
            }
        }
    }
}
