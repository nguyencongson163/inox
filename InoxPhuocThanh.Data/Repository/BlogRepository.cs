﻿using Dapper;
using InoxPhuocThanh.Data.Implement;
using InoxPhuocThanh.Data.Interface;
using InoxPhuocThanh.Data.ModelData;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace InoxPhuocThanh.Data.Repository
{
    public class BlogRepository : IBlogRepository
    {
        public List<BlogDataModel> getListBlog()
        {
            using (var connection = ConnectionProvider.Get())
            {
                var model = connection.Query<BlogDataModel>("sp_getListBlog", commandType: CommandType.StoredProcedure).ToList();
                return model;
            }
        }
        public List<BlogDataModel> getBlogById(int Id, int pageSize, int pageNum)
        {
            using (var connection = ConnectionProvider.Get())
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@blog_id", Id);
                param.Add("@pageSize", pageSize);
                param.Add("@pageNumber", pageNum);

                var model = connection.Query<BlogDataModel>("sp_getBlogById", param, commandType: CommandType.StoredProcedure).ToList();
                return model;
            }
        }
    }
}
