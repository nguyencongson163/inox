﻿using Dapper;
using InoxPhuocThanh.Data.Implement;
using InoxPhuocThanh.Data.Interface;
using InoxPhuocThanh.Data.ModelData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Data.Repository
{
    public class CompanyRepository : ICompanyRepository
    {
        public List<CompanyDataModel> GetCompanyInformation()
        {
            using (var connection = ConnectionProvider.Get())
            {
                return connection.Query<CompanyDataModel>("sp_getCompanyInformation", commandType: CommandType.StoredProcedure).ToList();
            }
        }
    }
}
