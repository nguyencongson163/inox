﻿using InoxPhuocThanh.Data.ModelData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Data.Interface
{
   public interface ICompanyRepository
    {
        List<CompanyDataModel> GetCompanyInformation();
    }
}
