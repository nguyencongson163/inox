﻿using InoxPhuocThanh.Data.ModelData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Data.Interface
{
   public interface IProductRepository
    {
        List<ProductDataModel> GetListProduct();
        List<ProductDataModel> GetListProductByCategoryOrTypeName(string name,int category, int type, int pageSize, int pageNum);
        List<ProductDataModel> GetListProductByCategoryName(String name, int pageSize, int pageNum);
        List<ProductDataModel> GetListProductByTypeName(String name, int pageSize, int pageNum);
        ProductDataModel GetProductDetail(int id);
        List<ProductDataModel> GetListProductNew();
        List<CategoryDataModel> GetListProductHome();
        List<ProductDataModel> SearchProduct(String searchValue, int categorySearch, int pageSize, int pageNum);
    }
}
