﻿using InoxPhuocThanh.Data.ModelData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Data.Interface
{
   public interface IBlogRepository
    {
        List<BlogDataModel> getListBlog();
        List<BlogDataModel> getBlogById(int Id, int pageSize, int pageNum);
    }
}
