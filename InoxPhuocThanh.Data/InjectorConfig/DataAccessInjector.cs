﻿using InoxPhuocThanh.Data.Interface;
using InoxPhuocThanh.Data.Repository;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InoxPhuocThanh.Data.InjectorConfig
{
    public static class DataAccessInjector
    {
        public static void Inject(Container container)
        {
            container.Register<IProductRepository, ProductRepository>();
            container.Register<ICategoryRepository, CategoryRepository>();
            container.Register<IProductTypeRepository, ProductTypeRepository>();
            container.Register<ICompanyRepository, CompanyRepository>();
            container.Register<IBlogRepository, BlogRepository>();
        }
    }
}
