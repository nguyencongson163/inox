﻿using System.Web;
using System.Web.Optimization;

namespace InoxPhuocThanh
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/js/jquery-3.4.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/js/vendor/modernizr-2.8.3.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/js/bootstrap.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                     "~/Scripts/js/jquery-page.js",
                      "~/Scripts/js/jquery.nivo.slider.pack.js",
                      "~/Scripts/js/jquery.meanmenu.min.js",
                      "~/Scripts/js/owl.carousel.min.js",
                      "~/Scripts/js/jquery-price-slider.js",
                      "~/Scripts/js/plugins.js",
                      "~/Scripts/js/jquery-page.js",
                      "~/Scripts/js/main.js"
                     ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap.min.css",
                      "~/Content/css/font-awesome.min.css",
                      "~/Content/css/meanmenu.min.css",
                      "~/Content/css/owl.carousel.css",
                      "~/Content/css/owl.theme.css",
                      "~/Content/css/owl.transitions.css",
                      "~/Content/css/jquery-ui.min.css",
                      "~/Content/css/nivo-slider.css",
                      "~/Content/css/animate.css",
                      "~/Content/css/jquery-ui-slider.css",
                      "~/Content/css/normalize.css",
                      "~/Content/css/main.css",
                      "~/Content/css/style.css",
                      "~/Content/css/responsive.css"
                      ));
        }
    }
}
