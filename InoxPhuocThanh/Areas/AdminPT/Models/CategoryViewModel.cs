﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InoxPhuocThanh.Areas.AdminPT.Models
{
    public class CategoryViewModel
    {
        public int Category_id { get; set; }

        [Required(ErrorMessage = "Vui lòng điền tên danh mục")]
        [Display(Name = "Tên Danh Mục")]
        public string Category_name { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateCreated { get; set; }
        public string Icon { get; set; }
        public List<ProductTypeViewModel> ProductType { get; set; }

    }
}