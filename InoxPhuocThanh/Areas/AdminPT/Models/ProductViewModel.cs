﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InoxPhuocThanh.Areas.AdminPT.Models
{
    public class ProductViewModel
    {
        public int Product_id { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn danh mục")]
        [Display(Name = "Danh mục sản phẩm")]
        public int Product_category_id { get; set; }
        public string Category_name { get; set; }

        public int Product_type_id { get; set; }
      
        public string Product_type_name { get; set; }

        [StringLength(100)]
        [Required(ErrorMessage = "Vui lòng nhập tên sản phẩm")]
        [Display(Name = "Tên sản phẩm")]
        public string Product_name { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập giá sản phẩm")]
        [Display(Name = "Giá sản phẩm")]
        public float Product_price { get; set; }
        //[StringLength(50)]
        //[Required(ErrorMessage = "Vui lòng thêm hình ảnh sản phẩm")]
        //[Display(Name = "Hình sản phẩm")]
        public string Product_image { get; set; }
        public DateTime Product_created_date { get; set; }
        [StringLength(200)]
        [Required(ErrorMessage = "Vui lòng thêm chi tiết sản phẩm")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Chi tiết")]
        public string Product_description { get; set; }
        [Required(ErrorMessage = "Vui lòng thêm số lượng")]
        [Display(Name = "Số lượng")]
        public int Product_quantity { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
        public int CountNum { get; set; }
        public string Price { get; set; }

    }
}