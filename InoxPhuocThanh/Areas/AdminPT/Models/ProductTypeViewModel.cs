﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InoxPhuocThanh.Areas.AdminPT.Models
{
    public class ProductTypeViewModel
    {
        public int Product_type_id { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên danh mục con")]
        [Display(Name = "Tên danh mục con")]
        public string Product_type_name { get; set; }
        [Required(ErrorMessage = "Vui lòng chọn danh mục")]
        [Display(Name = "Danh mục")]
        public int Category_id { get; set; }
        public int Product_quantity { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateCreate { get; set; }
    }
}