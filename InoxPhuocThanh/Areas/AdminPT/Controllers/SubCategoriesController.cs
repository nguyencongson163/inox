﻿using Dapper;
using InoxPhuocThanh.Areas.AdminPT.Implement;
using InoxPhuocThanh.Areas.AdminPT.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InoxPhuocThanh.Areas.AdminPT.Controllers
{
    public class SubCategoriesController : Controller
    {
        // GET: AdminPT/SubCategories
        public ActionResult Index()
        {
            using (var connection = ConnectionProvider.Get())
            {
                var model = connection.QueryMultiple("sp_admin_getListCateProType", commandType: CommandType.StoredProcedure);
                var categories = model.Read<CategoryViewModel>().ToList();

                var product = model.Read<ProductTypeViewModel>().ToList();
                categories.ForEach(x =>
                {
                    x.ProductType = product.Where(c => x.Category_id == c.Category_id).ToList();
                });
                ViewData["checkCate"] = 0;
                return View(categories);
            }
        }
        [HttpGet]
        public ActionResult SubIndex(int Id)
        {
            using (var connection = ConnectionProvider.Get())
            {
                var model = connection.QueryMultiple("sp_admin_getListCateProTypeById", commandType: CommandType.StoredProcedure);
                var categories = model.Read<CategoryViewModel>().ToList();
                var product = model.Read<ProductTypeViewModel>().ToList();
                categories.ForEach(x =>
                {
                    x.ProductType = product.Where(c => c.Category_id == x.Category_id).ToList();
                });
                ViewData["checkCate"] = Id;
                return View("Index",categories);
            }
        }
        public ActionResult Insert(int Id)
        {
            using (var connection = ConnectionProvider.Get())
            {
                var model = connection.QueryMultiple("sp_admin_getListCategory", commandType: CommandType.StoredProcedure);
                var categories = model.Read<CategoryViewModel>().Where(x => x.IsActive == true).ToList();
                //ViewData["checkCate"] = Id;
                Session["checkCate"] = Id;
                ViewData["categories"] = categories;
                return View();
            }
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Insert(ProductTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var connection = ConnectionProvider.Get())
                {
                    var isSuccess = connection.Execute("sp_admin_inserSubCate", new
                    {
                        model.Product_type_name,
                        model.Category_id,
                        model.IsActive
                    }, commandType: CommandType.StoredProcedure) > 0;
                    return RedirectToAction("SubIndex", "SubCategories", new { Id = model.Category_id });
                }

            }
            using (var connection = ConnectionProvider.Get())
            {
                var models = connection.QueryMultiple("sp_admin_getListCategory", commandType: CommandType.StoredProcedure);
                var categories = models.Read<CategoryViewModel>().Where(x => x.IsActive == true).ToList();
                //ViewData["checkCate"] = Id;
                ViewData["categories"] = categories;
                return View(model);
            }
        }
        [HttpGet]
        public ActionResult Detail(int Id =1,int cateId=1) {
            var model = new ProductTypeViewModel();
            var categories = new List<CategoryViewModel>();
            using (var connection = ConnectionProvider.Get())
            {
                model = connection.Query<ProductTypeViewModel>("sp_admin_getProductTypeById",new {
                    Id,
                    cateId
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
               
            }
            using (var connection = ConnectionProvider.Get())
            {
               var cate = connection.QueryMultiple("sp_admin_getListCategory", commandType: CommandType.StoredProcedure);
               categories = cate.Read<CategoryViewModel>().Where(x => x.IsActive == true).ToList();
            }
            ViewData["Categories"] = categories;
            return View(model);
        }
        [HttpGet]
        public ActionResult RemoveSubCate(int Id, int cateId)
        {
            using (var connection = ConnectionProvider.Get())
            {
                var isSuccess = connection.Execute("sp_admin_deleteSubCate", new
                {
                    Id,
                    cateId
                }, commandType: CommandType.StoredProcedure) > 0;
            }
            return RedirectToAction("SubIndex", "SubCategories", new { Id = cateId }); 
        }
        [HttpPost]
        public ActionResult UpdateSubCate(ProductTypeViewModel model)
        {
            using (var connection = ConnectionProvider.Get())
            {
                var isSuccess = connection.Execute("sp_admin_updateSubCate", new
                {
                    model.Product_type_id,
                    model.Product_type_name,
                    model.Category_id,
                    model.Product_quantity,
                    model.IsActive
                }, commandType: CommandType.StoredProcedure) > 0;
                return RedirectToAction("SubIndex", "SubCategories", new { Id = model.Category_id });
            }
        }

    }
}