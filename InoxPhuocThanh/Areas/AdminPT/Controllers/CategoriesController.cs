﻿using Dapper;
using InoxPhuocThanh.Areas.AdminPT.Implement;
using InoxPhuocThanh.Areas.AdminPT.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InoxPhuocThanh.Areas.AdminPT.Controllers
{

    public class CategoriesController : Controller
    {
        // GET: AdminPT/Categories
        public ActionResult Index()
        {
            using (var connection = ConnectionProvider.Get())
            {
                var model = connection.QueryMultiple("sp_admin_getListCategory", commandType: CommandType.StoredProcedure);
                var categories = model.Read<CategoryViewModel>().ToList();
                return View(categories);
            }
        }
        public ActionResult Insert()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Insert(CategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var connection = ConnectionProvider.Get())
                {
                    var isSuccess = connection.Execute("sp_admin_inserCate", new
                    {
                        CateName = model.Category_name,
                        model.IsActive,
                        model.Icon
                    }, commandType: CommandType.StoredProcedure) > 0;
                }
                return RedirectToAction("Index", "Categories");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult RemoveCate(int Id)
        {
            using (var connection = ConnectionProvider.Get())
            {
                var isSuccess = connection.Execute("sp_admin_deleteCate", new
                {
                    Id
                }, commandType: CommandType.StoredProcedure) > 0;
            }
            return RedirectToAction("Index", "Categories");
        }
        [HttpGet]
        public ActionResult DetailCate(int Id)
        {
            using (var connection = ConnectionProvider.Get())
            {
                var cate = connection.Query<CategoryViewModel>("sp_admin_detailCate", new
                {
                    Id
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return View("Detail", cate);
            }            
        }
        [HttpPost]
        public ActionResult UpdateCate(CategoryViewModel model)
        {
            using (var connection = ConnectionProvider.Get())
            {
                var isSuccess = connection.Execute("sp_admin_updateCate", new
                {
                  model.Category_id,
                  model.Category_name,
                  model.Icon,
                  model.IsActive
                }, commandType: CommandType.StoredProcedure) > 0;
            }
            return RedirectToAction("Index", "Categories");
        }
    }
}