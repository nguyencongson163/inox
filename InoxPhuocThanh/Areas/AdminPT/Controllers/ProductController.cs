﻿using Dapper;
using InoxPhuocThanh.Areas.AdminPT.Implement;
using InoxPhuocThanh.Areas.AdminPT.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InoxPhuocThanh.Areas.AdminPT.Controllers
{
    public class ProductController : Controller
    {
        // GET: AdminPT/Product
        public ActionResult Index()
        {
            using (var connection = ConnectionProvider.Get())
            {
                var model = connection.QueryMultiple("sp_admin_getListProduct", commandType: CommandType.StoredProcedure);
                var product = model.Read<ProductViewModel>().ToList();
                var cate = connection.QueryMultiple("sp_admin_getListCategory", commandType: CommandType.StoredProcedure);
                var categories = cate.Read<CategoryViewModel>().Where(x => x.IsActive == true).ToList();
                var proType = connection.QueryMultiple("sp_admin_getProductType", commandType: CommandType.StoredProcedure);
                var productTypes = proType.Read<ProductTypeViewModel>().ToList();
                ViewData["categories"] = categories;
                ViewData["productTypes"] = productTypes;
                return View(product);
            }
        }
        public ActionResult Insert()
        {
            using (var connection = ConnectionProvider.Get())
            {
                var cate = connection.QueryMultiple("sp_admin_getListCategory", commandType: CommandType.StoredProcedure);
                var categories = cate.Read<CategoryViewModel>().Where(x => x.IsActive == true).ToList();
                var proType = connection.QueryMultiple("sp_admin_getProductType", commandType: CommandType.StoredProcedure);
                var productTypes = proType.Read<ProductTypeViewModel>().ToList();
                ViewData["categories"] = categories;
                ViewData["productTypes"] = productTypes;
                return View();
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Insert(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (var connection = ConnectionProvider.Get())
                {
                    var IsSucess = connection.Execute("sp_admin_InsertProduct", new
                    {
                        model.Product_category_id,
                        model.Product_type_id,
                        model.Product_name,
                        model.Product_price,
                        model.Product_quantity,
                        model.Product_image,
                        model.Product_description,
                        model.IsActive

                    }, commandType: CommandType.StoredProcedure) > 0;
                }
                return RedirectToAction("Index", "Product");
            }
            using (var connection = ConnectionProvider.Get())
            {
                var cate = connection.QueryMultiple("sp_admin_getListCategory", commandType: CommandType.StoredProcedure);
                var categories = cate.Read<CategoryViewModel>().Where(x => x.IsActive == true).ToList();
                var proType = connection.QueryMultiple("sp_admin_getProductType", commandType: CommandType.StoredProcedure);
                var productTypes = proType.Read<ProductTypeViewModel>().ToList();
                ViewData["categories"] = categories;
                ViewData["productTypes"] = productTypes;
                return View(model);
            }
        }
        public ActionResult DeleteProduct(int Id)
        {
            var IsSucess = false;
            using (var connection = ConnectionProvider.Get()) {
                 IsSucess = connection.Execute("sp_admin_deleteProduct", new
                {
                    Id
                }, commandType: CommandType.StoredProcedure) >0;
            }
            return Json(new { success = IsSucess }, JsonRequestBehavior.AllowGet);
        }
    }
}
