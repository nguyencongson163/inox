﻿using System.Web.Mvc;

namespace InoxPhuocThanh.Areas.AdminPT
{
    public class AdminPTAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AdminPT";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AdminPT_default",
                "AdminPT/{controller}/{action}/{id}",
                new { controller = "DashBoard", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "InoxPhuocThanh.Areas.AdminPT.Controllers" }
            );
        }
    }
}