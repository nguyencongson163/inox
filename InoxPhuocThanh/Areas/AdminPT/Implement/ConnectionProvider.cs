﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace InoxPhuocThanh.Areas.AdminPT.Implement
{
    public class ConnectionProvider
    {
        public static IDbConnection Get()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString);
        }
    }
}