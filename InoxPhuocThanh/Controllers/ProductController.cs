﻿using AutoMapper;
using InoxPhuocThanh.Models;
using InoxPhuocThanh.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InoxPhuocThanh.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        public ProductController(IProductService productService, IMapper mapper)
        {
            this._productService = productService;
            this._mapper = mapper;
        }
        // GET: Product
        public ActionResult Index(string searchValue = "", int category = 0, int type = 0, int pageNum = 1)
        {
            int pageSize = 2;
            Session.Add("name", searchValue);
                Session.Add("type", type);
                Session.Add("category", category);
            if (pageNum.ToString() == null)
            {
                pageNum = 1;
            }
            List<ProductViewModel> model;
            model = _mapper.Map<List<ProductViewModel>>(_productService.GetListProductByCategoryOrTypeName(searchValue, category, type, pageSize, pageNum));
            foreach(ProductViewModel product in model)
                product.Price = product.Product_price.ToString("N0");
            if(model[0].CountNum % pageSize == 0)
            {
                model[0].PageNumber = model[0].CountNum / pageSize;
            } else
            {
                model[0].PageNumber = 1.0 * Convert.ToDouble(model[0].CountNum) / Convert.ToDouble(pageSize);
                if((model[0].PageNumber * 10) % 10 > 0)
                {
                    model[0].PageNumber = Convert.ToInt32(model[0].PageNumber) + 1;
                }else
                {
                    model[0].PageNumber = model[0].PageNumber;
                }
            }
            return View(model);
        }
        public PartialViewResult SearchProduct(string searchValue, int categorySearch = 1, int pageNum = 1)
        {
            int pageSize = 2;
            if (searchValue == null)
            {
                searchValue = "";
            }
            Session.Add("categorySearch", categorySearch);
            Session.Add("name", searchValue);
            List<ProductViewModel> model;
            model = _mapper.Map<List<ProductViewModel>>(_productService.SearchProduct(searchValue, categorySearch, pageSize, pageNum));
            foreach (ProductViewModel product in model)
                product.Price = product.Product_price.ToString("N0");
            return PartialView("Index",model);
        }
    }
}