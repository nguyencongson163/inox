﻿using AutoMapper;
using InoxPhuocThanh.Models;
using InoxPhuocThanh.service.Interface;
using InoxPhuocThanh.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InoxPhuocThanh.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        public CategoryController(ICategoryService categoryService,IProductService productService, IMapper mapper)
        {
            this._categoryService = categoryService;
            this._productService = productService;
            this._mapper = mapper;
        }
        // GET: Category
        public ActionResult CategoryPartial()
        {
            List<CategoryViewModel> category;
            category = _mapper.Map<List<CategoryViewModel>>(_categoryService.GetCategories());
            return PartialView("_CategoryPartial", category);
        }

        public ActionResult CategorySearchPartial()
        {
            List<CategoryViewModel> category;
            category = _mapper.Map<List<CategoryViewModel>>(_categoryService.GetCategories());
            return PartialView("_CategorySearch_Partial", category);
        }

        public ActionResult GetProductByCategoryName(int category, int type, int pageNum = 1)
        {
            int pageSize = 2;
            if (category.ToString() == null)
            {
                Session.Add("name", type);
            }
           
            if (pageNum.ToString() == null)
            {
                pageNum = 1;
            }
            List<ProductViewModel> model;
            model = _mapper.Map<List<ProductViewModel>>(_productService.GetListProductByCategoryName("sss", pageSize, pageNum));
            return View("../Product/Index", model);
        }
        public ActionResult GetProductByTypeName(string name, int pageNum = 1)
        {
            int pageSize = 2;
            //var nameSession = (string)Session["name"];
            if (name == null)
            {
                name = "";
            }
            if (name != null)
            {
                Session.Add("name", name);
            }
            if (pageNum.ToString() == null)
            {
                pageNum = 1;
            }
            List<ProductViewModel> model;
            model = _mapper.Map<List<ProductViewModel>>(_productService.GetListProductByTypeName(name, pageSize, pageNum));
            return View("../Product/Index", model);
        }
       
     
    }
}