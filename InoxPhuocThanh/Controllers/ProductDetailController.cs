﻿using AutoMapper;
using InoxPhuocThanh.Models;
using InoxPhuocThanh.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InoxPhuocThanh.Controllers
{
    public class ProductDetailController : Controller
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        public ProductDetailController(IProductService productService, IMapper mapper)
        {
            this._productService = productService;
            this._mapper = mapper;
        }
        // GET: ProductDetail
        public ActionResult Index(int id)
        {
            if (id.ToString()==null)
            {
                id = 0;
            }
            ProductViewModel model;
            model = _mapper.Map<ProductViewModel>(_productService.GetProductDetail(id));
            model.Price = model.Product_price.ToString("N0");
            return View(model);
        }
    }
}