﻿using AutoMapper;
using InoxPhuocThanh.Data.ModelData;
using InoxPhuocThanh.Models;
using InoxPhuocThanh.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InoxPhuocThanh.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        public HomeController(IProductService productService, IMapper mapper)
        {
            this._productService = productService;
            this._mapper = mapper;
        }
        public ActionResult Index()
        {
            var model = _mapper.Map<List<CategoryViewModel>>(_productService.GetListProductHome());
            return View(model);
        }

        public ActionResult NewProductPartial()
        {
            var model = _mapper.Map<List<ProductViewModel>>(_productService.GetListProductNew());
            return PartialView("_NewProductPartial", model);
        }
    }
}