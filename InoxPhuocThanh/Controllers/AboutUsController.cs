﻿using AutoMapper;
using InoxPhuocThanh.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InoxPhuocThanh.Controllers
{
    public class AboutUsController : Controller
    {
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;
        public AboutUsController(ICompanyService companyService, IMapper mapper)
        {
            this._companyService = companyService;
            this._mapper = mapper;
        }
        // GET: AboutUs
        public ActionResult Index()
        {
            return View();
        }
    }
}