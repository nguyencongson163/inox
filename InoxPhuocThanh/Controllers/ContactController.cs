﻿ using AutoMapper;
using InoxPhuocThanh.Models;
using InoxPhuocThanh.Service.Interface;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.IO;
using System.Linq;

namespace InoxPhuocThanh.Controllers
{
    public class ContactController : Controller
    {
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;
        public ContactController(ICompanyService companyService, IMapper mapper)
        {
            this._companyService = companyService;
            this._mapper = mapper;
        }
        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CompanyInfoPartial()
        {
            List<CompanyViewModel> model;
            model = _mapper.Map<List<CompanyViewModel>>(_companyService.GetCompanyInformation());
            return PartialView("_PartialInforContact", model);
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ContactViewModel contact)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var mail = new SmtpClient("smtp.gmail.com", 25)
                    {
                        Credentials = new NetworkCredential("tramanhc9p@gmail.com", "fantramanh"),
                        EnableSsl = true
                    };
                    var message = new MailMessage();
                    message.From = new MailAddress("tramanhc9p@gmail.com");
                    message.ReplyToList.Add("tramanhc9p@gmail.com");
                    message.To.Add("congson1603@gmail.com");
                    message.Subject = contact.Title;
                    message.IsBodyHtml = true;
                    string htmlString = @"<html>" +
                          "<body>" +
                          "<p>Xin chào! Xưởng Inox Phước Thành,</p>" +
                          "<p>Tôi tên là: " + contact.Fullname + "</p>" +
                          "<p>Email: " + contact.Customer_email + "</p>" +
                          "<p>Số điện thoại: " + contact.PhoneNumber + "</p>" +
                          "<p>Nội dung: " + contact.Description + "</p>" +
                          "<p>Xin cảm ơn</p>" +
                          "<p>Khách hàng</p>" +
                          "</body>" +
                          "</html>";
                    message.Body = htmlString;
                    HttpFileCollectionBase files = Request.Files;
                    HttpPostedFileBase file;
                    string fname;
                    for (int i = 0; i < files.Count; i++)
                    {
                        file = files[i];
                        fname = file.FileName;
                        fname = Path.Combine(Server.MapPath("~/img/contact/"), fname);
                        file.SaveAs(fname);
                        message.Attachments.Add(new Attachment(file.InputStream, fname));
                    }
                    mail.Send(message);

                }
                catch (Exception ex)
                {
                    return Json(ex.Message + " Files failed");
                }
            }
            return View();
        }


    }
}
