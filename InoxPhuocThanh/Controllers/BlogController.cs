﻿using AutoMapper;
using InoxPhuocThanh.Models;
using InoxPhuocThanh.service.Interface;
using InoxPhuocThanh.Service.Interface;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace InoxPhuocThanh.Controllers
{
    public class BlogController : Controller
    {
        private readonly IBlogService _blogService;
        private readonly IMapper _mapper;
        public BlogController(IBlogService blogService, IMapper mapper)
        {
            this._blogService = blogService;
            this._mapper = mapper;
        }
        // GET: Blog
        public ActionResult Index(int Id = 0, int pageSize = 2, int pageNum = 0)
        {
            if(pageNum == 0)
            {
                pageNum = 1;
            }
            var model = _mapper.Map<List<BlogViewModel>>(_blogService.getBlogById(Id, pageSize, pageNum));
            if (model[0].CountNum % pageSize == 0)
            {
                model[0].PageNumber = model[0].CountNum / pageSize;
            }
            else
            {
                model[0].PageNumber = 1.0 * Convert.ToDouble(model[0].CountNum) / Convert.ToDouble(pageSize);
                if ((model[0].PageNumber * 10) % 10 > 0)
                {
                    model[0].PageNumber = Convert.ToInt32(model[0].PageNumber)+1;
                }
                else
                {
                    model[0].PageNumber = model[0].PageNumber;
                }
            }
            return View(model);
        }

        public PartialViewResult BlogPartial()
        {
            var model = _mapper.Map<List<BlogViewModel>>(_blogService.getListBlog());
            return PartialView("NewBlogPartial",model);
        }
    }
}