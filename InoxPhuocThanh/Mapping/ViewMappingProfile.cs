﻿
using AutoMapper;
using InoxPhuocThanh.Models;
using InoxPhuocThanh.service.ModelService;

namespace InoxPhuocThanh.Mapping
{
    public class ViewMappingProfile: Profile
    {
        public ViewMappingProfile()
        {
            CreateMap<ProductServiceModel, ProductViewModel>();
            CreateMap<ProductViewModel, ProductServiceModel>();

            CreateMap<CategoryServiceModel, CategoryViewModel>();
            CreateMap<CategoryViewModel, CategoryServiceModel>();

            CreateMap<ProductTypeServiceModel, ProductTypeViewModel>();
            CreateMap<ProductTypeViewModel, ProductTypeServiceModel>();

            CreateMap<CompanyServiceModel, CompanyViewModel>();
            CreateMap<CompanyViewModel, CompanyServiceModel>();

            CreateMap<ContactImageServiceModel, ContactImageViewModel>();
            CreateMap<ContactImageViewModel, ContactImageServiceModel>();

            CreateMap<ContactViewModel, ContactServiceModel>();
            CreateMap<ContactServiceModel, ContactViewModel>();

            CreateMap<BlogViewModel, BlogServiceModel>();
            CreateMap<BlogServiceModel, BlogViewModel>();
        }
    }
}