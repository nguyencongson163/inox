﻿using AutoMapper;
using InoxPhuocThanh.Data.InjectorConfig;
using InoxPhuocThanh.Mapping;
using InoxPhuocThanh.service;
using InoxPhuocThanh.service.InjectorConfig;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace InoxPhuocThanh
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            var container = new Container();
            ServiceInjector.Inject(container);
            DataAccessInjector.Inject(container);
            // Automapper
            container.RegisterSingleton(() => GetMapper(container));
            container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
        private IMapper GetMapper(Container container)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ServiceMappingProfile());
                cfg.AddProfile(new ViewMappingProfile());
            });
            var mp = new Mapper(mapperConfig);
            return mp;
        }
    }
}
