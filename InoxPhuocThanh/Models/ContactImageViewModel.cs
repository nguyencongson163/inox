﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InoxPhuocThanh.Models
{
    public class ContactImageViewModel
    {
        public int Contact_img_id { get; set; }
        public int Contact_id { get; set; }
        public string Image { get; set; }
    }
}