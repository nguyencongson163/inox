﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InoxPhuocThanh.Models
{
    public class CategoryViewModel
    {
        public int Category_id { get; set; }
        public string Category_name { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
        public List<ProductTypeViewModel> ProductType { get; set; }
        public List<ProductViewModel> Product { get; set; }
    }
}