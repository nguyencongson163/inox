﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InoxPhuocThanh.Models
{
    public class ProductImageViewModel
    {
        public int Product_Img_Id { get; set; }
        public int Product_Id { get; set; }
        public string Product_Name { get; set; }
        public string Product_Image { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
    }
}