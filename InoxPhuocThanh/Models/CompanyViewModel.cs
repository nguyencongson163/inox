﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InoxPhuocThanh.Models
{
    public class CompanyViewModel
    {
        public int Company_id { get; set; }
        public string Company_name { get; set; }
        public string Company_description { get; set; }
        public string Company_address { get; set; }
        public string Company_phonenumber1 { get; set; }
        public string Company_phonenumber2 { get; set; }
        public string Company_gmail { get; set; }
        public string Company_facebook { get; set; }
        public string Company_zalo { get; set; }
        public string Company_time_working { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
    }
}