﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InoxPhuocThanh.Models
{
    public class ContactViewModel
    {
        public int Contact_id { get; set; }
        [StringLength(50)]
        [Required(ErrorMessage = "Vui lòng nhập họ và tên")]
        [Display(Name = "Họ và tên")]
        public string Fullname { get; set; }
        [StringLength(50)]
        [Required(ErrorMessage = "Vui lòng nhập số điện thoại")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Số điện thoại")]
        public string PhoneNumber { get; set; }
        [StringLength(50)]
        [Required(ErrorMessage = "Vui lòng nhập chủ đề")]
        [Display(Name = "Chủ đề")]
        public string Title { get; set; }
        [DataType(DataType.MultilineText)]
        [Display(Name = "Yêu cầu")]
        public string Description { get; set; }
        [StringLength(50)]
        [Required(ErrorMessage = "Vui lòng nhập mail")]
        [EmailAddress]
        [Display(Name ="Email")]
        public string Customer_email { get; set; }
        public DateTime Created_date { get; set; }
    }
}